"" Mike Messmore's init.vim

" auto-install vim-plug
if empty(glob('~/.config/nvim/autoload/plug.vim'))
  silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall
endif
call plug#begin()
Plug 'scrooloose/syntastic'
Plug 'scrooloose/nerdcommenter'
Plug 'bling/vim-airline'
Plug 'kien/ctrlp.vim'
Plug 'altercation/vim-colors-solarized'
Plug 'elzr/vim-json'
Plug 'plasticboy/vim-markdown'
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'tpope/vim-fugitive'
Plug 'rodjek/vim-puppet'
Plug 'godlygeek/tabular'
Plug 'pangloss/vim-javascript'
Plug 'leafgarland/typescript-vim'
Plug 'guns/vim-clojure-static'
Plug 'fatih/vim-go'
Plug 'vim-airline/vim-airline-themes'
call plug#end()

"vim-markdown
let g:vim_markdown_folding_disabled = 1

"nerdcommenter
let g:NERDSpaceDelims = 1

"syntastic
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_aggregate_errors = 1
let g:syntastic_python_checkers = ['flake8', 'python']

"deocomplete
" Disable AutoComplPop.
let g:acp_enableAtStartup = 0
" Use deocomplete.
let g:deocomplete#enable_at_startup = 1
" Use smartcase.
let g:deocomplete#enable_smart_case = 1
" Set minimum syntax keyword length.
let g:deocomplete#sources#syntax#min_keyword_length = 3
"VUNDLE
" Define dictionary.
let g:deocomplete#sources#dictionary#dictionaries = {
    \ 'default' : '',
    \ 'vimshell' : $HOME.'/.vimshell_hist',
    \ 'scheme' : $HOME.'/.gosh_completions'
        \ }
"VUNDLE
" Define keyword.
if !exists('g:deocomplete#keyword_patterns')
    let g:deocomplete#keyword_patterns = {}
endif
let g:deocomplete#keyword_patterns['default'] = '\h\w*'
"VUNDLE
" Plugin key-mappings.
inoremap <expr><C-g>     deocomplete#undo_completion()
inoremap <expr><C-l>     deocomplete#complete_common_string()
"VUNDLE
" Recommended key-mappings.
" <CR>: close popup and save indent.
inoremap <silent> <CR> <C-r>=<SID>my_cr_function()<CR>
function! s:my_cr_function()
  return (pumvisible() ? "\<C-y>" : "" ) . "\<CR>"
  " For no inserting <CR> key.
  "return pumvisible() ? "\<C-y>" : "\<CR>"
endfunction
" <TAB>: completion.
inoremap <expr><TAB>  pumvisible() ? "\<C-n>" : "\<TAB>"
" <C-h>, <BS>: close popup and delete backword char.
inoremap <expr><C-h> deocomplete#smart_close_popup()."\<C-h>"
inoremap <expr><BS> deocomplete#smart_close_popup()."\<C-h>"
" Close popup by <Space>.
"inoremap <expr><Space> pumvisible() ? "\<C-y>" : "\<Space>"
"VUNDLE
" AutoComplPop like behavior.
"let g:deocomplete#enable_auto_select = 1
"VUNDLE
" Shell like behavior(not recommended).
"set completeopt+=longest
"let g:deocomplete#enable_auto_select = 1
"let g:deocomplete#disable_auto_complete = 1
"inoremap <expr><TAB>  pumvisible() ? "\<Down>" : "\<C-x>\<C-u>"
"VUNDLE
" Enable omni completion.
autocmd FileType css setlocal omnifunc=csscomplete#CompleteCSS
autocmd FileType html,markdown setlocal omnifunc=htmlcomplete#CompleteTags
autocmd FileType javascript setlocal omnifunc=javascriptcomplete#CompleteJS
autocmd FileType python setlocal omnifunc=pythoncomplete#Complete
autocmd FileType xml setlocal omnifunc=xmlcomplete#CompleteTags
"VUNDLE
" Enable heavy omni completion.
if !exists('g:deocomplete#sources#omni#input_patterns')
  let g:deocomplete#sources#omni#input_patterns = {}
endif
"let g:deocomplete#sources#omni#input_patterns.php = '[^. \t]->\h\w*\|\h\w*::'
"let g:deocomplete#sources#omni#input_patterns.c = '[^.[:digit:] *\t]\%(\.\|->\)'
"let g:deocomplete#sources#omni#input_patterns.cpp = '[^.[:digit:] *\t]\%(\.\|->\)\|\h\w*::'
"VUNDLE
" For perlomni.vim setting.
" https://github.com/c9s/perlomni.vim
let g:deocomplete#sources#omni#input_patterns.perl = '\h\w*->\h\w*\|\h\w*::'
"VUNDLE

" theme
let g:airline_theme='papercolor'



"" GENERAL STUFF
" color, etc.
filetype plugin indent on
set background=dark
" keep the ruler on the screen
set ruler
" don't beep
set noerrorbells
set modeline
set laststatus=2
set tabstop=4 shiftwidth=4 expandtab


" Delete trailing white space on save, keep KSB happy :)
func! DeleteTrailingWS()
	exe "normal mz"
	%s/\s\+$//ge
	exe "normal `z"
endfunc
au BufWrite * :call DeleteTrailingWS()
"" SPECIFIC STUFF
" Make sure arduino stuff gets C highlighting
au BufNewFile,BufRead *.ino set filetype=c
" For man pages
au BufNewFile,BufRead *.man set filetype=groff
" Wrapping for LaTeX
au BufNewFile,BufRead *.tex set textwidth=75
" Don't expand tabs to spaces where inappropriate
au FileType make,sh setlocal noexpandtab
" Use 2 spaces on Puppet (ick!) because convention
" Use 2 spaces on HTML because HTML
" Use 2 spaces on Ruby because style
au FileType puppet,html,ruby,yaml,typescript set tabstop=2 shiftwidth=2 expandtab
