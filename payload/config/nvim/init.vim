"" Mike Messmore's init.vim

" auto-install vim-plug
if empty(glob('~/.config/nvim/autoload/plug.vim'))
  silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall
endif
call plug#begin()
Plug 'scrooloose/syntastic'
Plug 'scrooloose/nerdcommenter'
Plug 'bling/vim-airline'
Plug 'kien/ctrlp.vim'
Plug 'altercation/vim-colors-solarized'
Plug 'elzr/vim-json'
Plug 'plasticboy/vim-markdown'
Plug 'tpope/vim-fugitive'
Plug 'rodjek/vim-puppet'
Plug 'godlygeek/tabular'
Plug 'pangloss/vim-javascript'
Plug 'leafgarland/typescript-vim'
Plug 'guns/vim-clojure-static'
Plug 'fatih/vim-go'
Plug 'vim-airline/vim-airline-themes'

Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'zchee/deoplete-go', { 'do': 'make'}
Plug 'zchee/deoplete-jedi'
Plug 'mhartington/deoplete-typescript'
Plug 'carlitux/deoplete-ternjs', { 'do': 'npm install -g tern' }
Plug 'Shougo/neopairs.vim'
call plug#end()

"vim-markdown
let g:vim_markdown_folding_disabled = 1

"nerdcommenter
let g:NERDSpaceDelims = 1

"syntastic
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_aggregate_errors = 1
let g:syntastic_python_checkers = ['flake8', 'python']


" theme
let g:airline_theme='papercolor'

" Use deoplete.
let g:deoplete#enable_at_startup = 1
" omnifuncs
augroup omnifuncs
  autocmd!
  autocmd FileType css setlocal omnifunc=csscomplete#CompleteCSS
  autocmd FileType html,markdown setlocal omnifunc=htmlcomplete#CompleteTags
  autocmd FileType javascript setlocal omnifunc=javascriptcomplete#CompleteJS
  autocmd FileType python setlocal omnifunc=pythoncomplete#Complete
  autocmd FileType xml setlocal omnifunc=xmlcomplete#CompleteTags
augroup end
" deoplete tab-complete
inoremap <expr><tab> pumvisible() ? "\<c-n>" : "\<tab>"

" deoplete python stuff
let g:python_host_prog = '/usr/local/bin/python'
let g:python3_host_prog = '/usr/local/bin/python3'


"" GENERAL STUFF
" color, etc.
filetype plugin indent on
set background=dark
" keep the ruler on the screen
set ruler
" don't beep
set noerrorbells
set modeline
set laststatus=2
set tabstop=4 shiftwidth=4 expandtab


" Delete trailing white space on save, keep KSB happy :)
func! DeleteTrailingWS()
	exe "normal mz"
	%s/\s\+$//ge
	exe "normal `z"
endfunc
au BufWrite * :call DeleteTrailingWS()
"" SPECIFIC STUFF
" Make sure arduino stuff gets C highlighting
au BufNewFile,BufRead *.ino set filetype=c
" For man pages
au BufNewFile,BufRead *.man set filetype=groff
" Wrapping for LaTeX
au BufNewFile,BufRead *.tex set textwidth=75
" Don't expand tabs to spaces where inappropriate
au FileType make,sh setlocal noexpandtab
" Use 2 spaces on Puppet (ick!) because convention
" Use 2 spaces on HTML because HTML
" Use 2 spaces on Ruby because style
au FileType puppet,html,ruby,yaml,typescript set tabstop=2 shiftwidth=2 expandtab
