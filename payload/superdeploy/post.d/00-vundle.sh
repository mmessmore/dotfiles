#!/bin/bash

[ -f ~/.proxy ] && . ~/.proxy
SED=sed
which gsed >/dev/null 2>&1 && SED=gsed

if [ -d ~/.vim/bundle/Vundle.vim ]; then
	$SED -i "s/^\"VUNDLE //;s/^\"$(uname -n) VUNDLE //" ~/.vimrc
	echo -e "\n Run :PluginUpdate or :PluginInstall in vim"
	# This gets weird too much
    # vim -i NONE -c PluginUpdate -c PluginClean -c quitall > /dev/null 2>&1
else
    git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
	$SED -i "s/^\"VUNDLE //;s/^\"$(uname -n) VUNDLE //" ~/.vimrc
	echo -e "\n Run :PluginUpdate or :PluginInstall in vim"
	# This gets weird too much
    # vim -i NONE -c PluginInstall -c quitall > /dev/null 2>&1
fi

if [ $(uname) != Darwin ]; then
	rm -fr ~/.iterm*
fi
