#!/bin/bash
default_nvim() {
	echo "Making neovim our vim/vi/editor"
	sudo update-alternatives --install /usr/bin/vi vi /usr/bin/nvim 60
	sudo update-alternatives --set vi /usr/bin/nvim
	sudo update-alternatives --install /usr/bin/vim vim /usr/bin/nvim 60
	sudo update-alternatives --set vim /usr/bin/nvim
	sudo update-alternatives --install /usr/bin/editor editor /usr/bin/nvim 60
	sudo update-alternatives --set editor /usr/bin/nvim
}
echo "Neovim setup"
if  [ -f /etc/debian_version ]; then
	real_vim=$(update-alternatives --display vim | tail -1 | awk '{print $1}')
	echo ${real_vim##*/}
	if [ ${real_vim##*/} = "vim.basic" ]; then
		default_nvim
	fi
fi
