# My Dotfile Madness

## What?
This includes all of my usual settings.  My .profile, my .vimrc, etc.

New files should be added to payload/ without a preceding '.'

`superdeploy` is a script to deploy the files, as well as a ssh-copy-id clone (without the
seemingly unnecessary selinux bits)

## Why?
This helps me keep track of stuff, and other people may benefit
